import React, { useState } from 'react';
import axios from 'axios';

import {Map} from '../components/organisms/Map';
import { WeatherTable } from '../components/molecules/WeatherTable';
import { SendForm } from '../components/molecules/SendFrom';

const MapContainer = () => {

    const [weather, setWeather] = useState({
        error: false,
        data: {

        }
    });

    const getWeather = async (lat, long) => {

        try {
            const data = await axios.get(`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&appid=${process.env.REACT_APP_API_KEY}`);

            if (!data.status || data.status !== 200) {
                throw 1;
            }

            setWeather({
                error: false,
                data: data.data,
            })
        }
        catch (e) {

            setWeather((data) => ({
                ...data,
                error: true,
            }));

        }
    }

    return (
        <div className="wrapper">
            <div className="column map">
                <Map getWeather={getWeather} />
            </div>
            <div className="column">
                <WeatherTable weather={weather} />
                <SendForm weather={weather} />
            </div>
        </div>
    );
}

export {MapContainer}
