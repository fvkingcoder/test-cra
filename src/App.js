import React from 'react';
import './App.css';

import {MapContainer} from './containers/MapContainer';

function App() {

  return (
    <MapContainer />
  );
}

export default App;
