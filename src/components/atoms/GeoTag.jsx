import React from 'react';


const GeoTag = props => {

    return (
        <div className="geo" style={{left: props.x-10, top: props.y-20}}>
            <img src="geo.png" />
        </div>
    );
}

export { GeoTag }