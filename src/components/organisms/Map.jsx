import React, { useState } from 'react';
import { useRef } from 'react';
import { GeoTag } from '../atoms/GeoTag';

const Map = props => {

    const mapImage = useRef(null);

    const [coords, setCoords] = useState({
        x: 0,
        y: 0,
    })

    const sdsd = (event) => {
      const bounds= mapImage.current.getBoundingClientRect();
      let left=bounds.left;
      let top=bounds.top;
      let x = event.pageX;
      let y = event.pageY;
      let cw=mapImage.current.clientWidth
      let ch=mapImage.current.clientHeight
      let iw=mapImage.current.naturalWidth
      let ih=mapImage.current.naturalHeight
      let px=x/cw*iw
      let py=y/ch*ih

      const initCoords = {
          lat: [56.961322, 54.255666],
          long: [35.144130, 40.203885],
      }
  
      const delta = {
          lat: (initCoords.lat[1]-initCoords.lat[0])/ch,
          long: (initCoords.long[1]-initCoords.long[0])/cw,
      }

      const lat = py*delta.lat+initCoords.lat[0];
      const long = px*delta.long+initCoords.long[0];

      props.getWeather(lat, long);

      setCoords({
          x: x,
          y: y,
      })
    }
  

    return (
        <>
            <GeoTag x={coords.x} y={coords.y} />
            <img src="sdsd.png" className="map" alt="Map" ref={mapImage} onClick={sdsd} />
        </>
    );
}

export {Map}