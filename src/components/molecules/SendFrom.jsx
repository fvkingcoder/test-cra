import React from 'react';
import { useForm } from "react-hook-form";

const SendForm = props => {

    const { register, handleSubmit, setError, errors, clearError } = useForm();

    const onSubmit = (data) => {

        clearError();

        if (typeof props.weather.data !== 'object' || !Object.keys(props.weather.data).length) {

            setError("weather", "empty", "weather is empty");
            
            return;
        }

        console.log(data.email, props.weather);

    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <input type="text"         
                   name="email"
                   ref={register({
                    required: "Required",
                    pattern: {
                        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                        message: "invalid email address"
                    }
                    })}
            />
            {errors.email && errors.email.message}
            {errors.weather && errors.weather.message}
            <button type="submit">Отправить</button>
        </form>
    )
}

export {SendForm}