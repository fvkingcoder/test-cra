import React from 'react';

const WeatherTable = props => {

    const renderTableBody = () => {

        const rows = [];
        if (!props.weather.data.main || props.weather.error) {
            return (
                <tr colSpan={2}>
                    <td>
                        {props.weather.error && 'Something went wrong'} 
                    </td>
                </tr>
            )
        }

        Object.keys(props.weather.data.main).map((val, index)=> {
            
            rows.push(
            (
                <tr key={`result${index}`}>
                    <td>{val}</td> 
                    <td>{props.weather.data.main[val]}</td> 
                </tr>
            ));
        });

        rows.push( 
            <tr key={`result_name`}>
                <td>name</td> 
                <td>{props.weather.data.name}</td> 
            </tr> 
        );

        return rows;
    }

    return (
        <table>
            <thead>
                <tr>
                    <th>name</th>
                    <th>val</th>
                </tr>
            </thead>
            <tbody>
                {renderTableBody()}
            </tbody>
        </table>
    )
}

export { WeatherTable }